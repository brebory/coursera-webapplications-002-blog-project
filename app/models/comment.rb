class Comment < ActiveRecord::Base
  belongs_to :post, :foreign_key => :post_id

  validates :post_id, :presence => true
  validates :body, :presence => true
end
